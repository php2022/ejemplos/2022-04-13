<?php

//Creo un objeto de tipo PDO (su clase es PDO)
//La variable conexion me permite comunicarme con el servidor de bbdd
$conexion=new PDO("mysql:host=127.0.0.1;dbname=usuarios","root","");

//Utilizo el metodo prepare de un objeto PDO 
//para ejecutar una consulta SQL
//la variable respuesta es la que tiene la consulta preparada
$respuesta=$conexion->prepare("select * from  usuarios");

//Ejecuto la consulta a traves del metodo execute
$respuesta->execute();

//En la variable usuarios tengo todos los registros
//en formato array mixto
//$usuarios[0]
$usuarios=$respuesta->fetchAll();

//Me crea un texto en formato JSON
//con todos los registros
$usuariosJson=json_encode($usuarios);

